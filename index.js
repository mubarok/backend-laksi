const express = require("express")
const app = express()
const bodyParser = require("body-parser") // to get data form post body
let routes = require("./src/routes")
let config = require("./src/configs")

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended:true }))

server = () => {
    routes.init(app)
}

startUp =  () => {
    let hostname = config.hostname;
    let port = config.port

    app.listen(port, () => {
        console.log('Initiate App, server-listen on - http://' + hostname + ':' + port)

        // process.on("uncaughtException", (e) => {
        //     process.exit(1)
        // })
    })
}

server();
startUp();