const express = require("express")
const router = express.Router()
const userApi = require("./../../modules/users/apiHandler")
const departemenApi = require("./../../modules/departemen/apiHandler")
const cabangApi = require("./../../modules/cabang/apiHandler")
const lokasiApi = require("./../../modules/lokasi/apiHandler")


router.use("/users", userApi)
router.use("/departemen", departemenApi)
router.use("/cabang", cabangApi)
router.use("/lokasi", lokasiApi)

module.exports = router