const apiRouter = require("./api/v1")

init = (app) => {

    app.get("/", (req, res) => {
        res.status(200).json({
            "message" : "App is Working"
        })
    })
    
    app.use("/api/v1", apiRouter)
}

module.exports = {
    init : init
}
