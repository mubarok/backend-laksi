const router = require("express").Router()
const controller = require("./controller")

router.get("/", async (req, res) => {
    
    const result = await controller.getAll()
    res.status(200).json({
        "status" : true,
        "code":200,
        "message": "Get data successfull",
        "data": result
    })
})

router.get("/:id",  async(req, res) => {
    
    const id = req.params.id
    const result = await controller.getID(id)

    res.status(200).json({
        "status" : true,
        "code":200,
        "message": "Get data successfull",
        "data": result
    })
    
})

router.post("/", async(req, res) => {

    const payload = {
        id_lokasi : req.body.id_lokasi,
        nama: req.body.nama,
        kode: req.body.kode
    }

    const result = await controller.save(payload)
    res.status(200).json({
        "status" : true,
        "code":200,
        "message": "The data has been created!",
    })
})

router.put("/:id", async(req, res) => {

    const payload = {
        id: req.params.id,
        id_lokasi: req.body.id_lokasi,
        nama: req.body.nama,
        kode: req.body.kode
    }

    const result = await controller.update(payload)
    res.status(200).json({
        "status" : true,
        "code":200,
        "message": "The data has been updated!",
    })

})

router.delete("/:id", async(req, res) => {

    const id = req.params.id
    const result = await controller.destroy(id)
    res.status(200).json({
        "status" : true,
        "code":200,
        "message": "The data has been deleted!",
    })
    
})

module.exports = router