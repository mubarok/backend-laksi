const Cabang = require("./Cabang")

const findAll = async() => {
    
    const result = await Cabang.findAll({include: ["lokasi"]})
    return result
}

const findID = async(id) => {
    
    const result = await Cabang.findByPk(id)
    return result
}

const save = async (payload) => {
    const result = await Cabang.create(payload)
    return result
}

const update = async(payload) => {
    
    const result = await Cabang.update(payload,{
        where: {
            id: payload.id
        }
    })
}

const destroy = async(id) => {

    const result = await Cabang.destroy({
        where:{
            id: id
        }
    })

    return result
}

module.exports = {
    findAll, findID, save, update, destroy
}