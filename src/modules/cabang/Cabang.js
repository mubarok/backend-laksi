const { Sequelize, DataTypes, Model} = require("sequelize")
// const Lokasi = require("../lokasi/Lokasi")
const sequelize = require("./../../configs/connection")

class Cabang extends Model{}

Cabang.init({
    id: {
        type : DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    id_lokasi: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    nama: {
        type: DataTypes.STRING,
        allowNull: true
    },
    kode: {
        type: DataTypes.STRING,
        allowNull: true
    }
},{
    sequelize,
    modelName: "mst_cabang",
    tableName: "mst_cabang",
    timestamps: false
})

// Cabang.hasOne(Lokasi,{
//     as: "lokasi",
//     onUpdate: "CASCADE"
// })

module.exports = Cabang