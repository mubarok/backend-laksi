const model = require("./model")

const getAll= async() => {
    const result = await model.findAll()
    return result
}

module.exports = {
    getAll
}