const router = require("express").Router()
const controller = require("./controller")

router.get("/", async (req, res) => {

    const result = await controller.getAll()

    return res.status(200).json({
        success: true,
        data: result,
        message: 'Get users success',
        code: 200
    });
})

module.exports = router