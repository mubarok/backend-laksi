const { Sequelize, DataTypes, Model } = require("sequelize")
const sequelize = require("../../configs/connection")

class User extends Model{}

User.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nama: {
        type: DataTypes.STRING,
        allowNull : false
    },
    email: {
        type: DataTypes.STRING,
        allowNull : false
    },
    username: {
        type: DataTypes.STRING,
        allowNull : false
    },
    token: {
        type: DataTypes.STRING,
        allowNull : false
    },
    aktif: {
        type: DataTypes.INTEGER,
        allowNull : false
    },
}, {
    sequelize,
    modelName : 'users',
    timestamps : false
});

// console.log(User === sequelize.models.User)

module.exports = User