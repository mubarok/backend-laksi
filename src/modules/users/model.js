const User = require("../users/User")

const findAll = async() => {
    const result = await User.findAll()
    return result
}

module.exports = {
    findAll
}