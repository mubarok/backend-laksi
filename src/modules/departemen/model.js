const Departemen = require("./Departemen")

const findAll = async() => {

    const result = await Departemen.findAll();
    return result
}

const findID = async(id) =>{

    const result = await Departemen.findByPk(id)
    return result
}

const save = async(payload) => {
    const result = await Departemen.create(payload)
    return result
}

const update = async(payload) =>{
    const result = await Departemen.update(payload,{ 
                where: { 
                        id: payload.id 
                    } 
                })

    return result
}

const destroy = async(id) => {
    const result = Departemen.destroy({
        where: {
            id: id
        }
    })

    return result
}

module.exports = {
    findAll, findID, save, update, destroy
}