const router = require("express").Router();
const controller = require("./controller")
const { responseTrue } = require("./formatRespon")

router.get("/", async(req, res) => {

    const result = await controller.getAll()
    return responseTrue(result, res)

})

router.get("/:id", async(req, res) => {

    let id = req.params.id
    const result = await controller.getID(id)
    return res.status(200).json({
        success: true,
        code: 200,
        message: 'Get departemen successfull',
        data: result
        
    })
})

router.post("/", async(req, res) =>{
    
    let payload = {
        nama: req.body.nama
    }
    const result = await controller.save(payload)
    res.status(200).json({
        "status": true,
        "code": 200,
        "message": "The data has been created!",
        "data": result
    })
})

router.put("/:id", async(req, res) => {
    
    const payload = {
        id: req.params.id,
        nama: req.body.nama
    }

    const result = await controller.update(payload)
    res.status(200).json({
        "status": true,
        "code": 200,
        "message": "The data has been updated!"
    })

})

router.delete("/:id", async(req, res) => {
    
    const id = req.params.id
    const result = await controller.destroy(id)

    res.status(200).json({
        "status": true,
        "code": 200,
        "message": "The data has been deleted!"
    })
})

module.exports = router