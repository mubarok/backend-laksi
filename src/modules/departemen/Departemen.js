const { Sequelize, DataTypes, Model } = require("sequelize")
const sequelize = require("./../../configs/connection")

class Departemen extends Model{}

Departemen.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey : true,
        autoIncrement: true
    },
    nama: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    parent : {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    is_deleted : {
        type: DataTypes.INTEGER,
        defaultValue: 0
    }
},{
    sequelize,
    modelName : "mst_departemen",
    tableName: "mst_departemen",
    timestamps: false
})

module.exports = Departemen