const model = require("./model")

const getAll = async() => {

    const result = await model.findAll()
    return result
}

const getID = async(id) => {

    const result = await model.findID(id)
    return result
}

const save = async(payload) => {

    const result = await model.save(payload)
    return result
}

const update = async(payload) => {

    const result = await model.update(payload)
    return result
}

const destroy = async(id) => {

    const result = await model.destroy(id)
    return result
}

module.exports = {
    getAll, getID, save, update, destroy
}