const Lokasi = require("./Lokasi")

const findAll = async() => {
    
    const result = await Lokasi.findAll({ include: ["cabang"]})
    return result
}

const findID = async(id) => {
    
    const result = await Lokasi.findByPk(id)
    return result
}

const save = async(payload) => {
    
    const result = await Lokasi.create(payload)
    return result
}

const update = async(payload) => {
    
    const result = await Lokasi.update(payload,{
                        where: {
                            id: payload.id
                        }
                    })
    return result
}

const destroy = async(id) => {
    
    const result = await Lokasi.destroy({
                        where: {
                            id: id
                        }
                    })
    return result
}


module.exports = {
    findAll, findID, save, update, destroy
}