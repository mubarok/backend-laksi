const { Sequelize, DataTypes, Model } = require("sequelize")
const sequelize = require("./../../configs/connection")
const Cabang = require("./../cabang/Cabang")

class Lokasi extends Model{}

Lokasi.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    nama: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    is_deleted: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    }
},{
    sequelize,
    modelName: "mst_lokasi",
    tableName: "mst_lokasi",
    timestamps: false
})

Lokasi.hasMany(Cabang,{
    as: "cabang",
    foreignKey: "id_lokasi"
})

// Cabang.belongsTo(Lokasi,{
//     as: "lokasi",
//     foreignKey: "id_lokasi"

// })


module.exports = Lokasi
