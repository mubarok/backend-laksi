const config = require("../configs"); // include config
const { Sequelize } = require("sequelize");

//connection
const sequelize = new Sequelize(config.mysql.mysqlDB, config.mysql.mysqlUsername, config.mysql.mysqlPassword, {
    host : config.hostname,
    dialect : 'mysql'
});


// testing the connection
 try {
    sequelize.authenticate();
    console.log('Connection has been established successfully.');
} catch (error) {
    console.error('Unable to connect to the database:', error);
}

module.exports = sequelize