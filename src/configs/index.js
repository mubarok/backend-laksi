require("dotenv").config({ path : __dirname+'/../../.env' })

const config = {
    env : process.env.env,
    hostname : process.env.DB_HOST,
    port:  process.env.PORT,
    mysql:{
        mysqlDB : process.env.DB_NAME,
        mysqlUsername : process.env.DB_USER,
        mysqlPassword : process.env.DB_PASSWORD
    }
}

module.exports = config